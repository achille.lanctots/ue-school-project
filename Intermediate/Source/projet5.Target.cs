using UnrealBuildTool;

public class projet5Target : TargetRules
{
	public projet5Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("projet5");
	}
}
